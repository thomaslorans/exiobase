# This file contains function to open the zip file.


"""
    Satellite
A data structure representing the Exiobase3 satellite accounts.
"""
Base.@kwdef mutable struct Satellite
    F::NamedArray # Factor production
    D_cba_reg::NamedArray # 
    D_exp_reg::NamedArray #
    D_imp_reg::NamedArray #
    D_pba_reg::NamedArray #
    D_cba::NamedArray # 
    D_pba::NamedArray #
    M::NamedArray # 
    S_Y::NamedArray # 
    S::NamedArray # 
    F_Y::NamedArray # 
    unit::DataFrame # Units
end

"""
    Impacts
A data structure reprensenting the Exiobase impacts accounts
"""
Base.@kwdef mutable struct Impacts
    F::NamedArray # Factor production
    D_cba_reg::NamedArray # 
    D_exp_reg::NamedArray #
    D_imp_reg::NamedArray #
    D_pba_reg::NamedArray #
    D_cba::NamedArray # 
    D_pba::NamedArray #
    M::NamedArray # 
    S_Y::NamedArray # 
    S::NamedArray # 
    F_Y::NamedArray #
    unit::DataFrame # Units
end

"""
    Exiobase3
A data structure representing the Exiobase3 data parsed.
"""
Base.@kwdef mutable struct Exiobase3
    Z::NamedArray # Transaction matrix
    Y::NamedArray # Final demand matrix
    x::NamedArray # Gross output
    A::NamedArray # Leontieff matrix
    satellite::Satellite # Satellite accounts 
    impacts::Impacts # Impacts accounts
    unit::DataFrame # Units
end


"""
    function loadData(file)

This function loads the data from Exiobase.

# Arguments
* `file`: The path to the file (unzipped) we want to load.

"""
function loadData(IOTfile::String)

    # Prepare remediation 
    remediationFiles = remediation(parseData(string(IOTfile, "/x.txt"), "Vector"))

    # Exiobase3 matrices
    x = transformToNamedArray(remediationFiles.x,"Vector")

    Z = transformToNamedArray(parseData(string(IOTfile, "/Z.txt"), "Matrix"),"SquareMatrix")
    Y = transformToNamedArray(parseData(string(IOTfile, "/Y.txt"), "Matrix"),"RectangularMatrix")
    A = transformToNamedArray(parseData(string(IOTfile, "/A.txt"), "Matrix"),"SquareMatrix")
    unit = CSV.read(string(IOTfile, "/unit.txt"), DataFrame)

    # Satellite accounts 
    F_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/F.txt"), "Matrix"),"RectangularMatrixAlt")
    D_cba_reg_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/D_cba_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_exp_reg_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/D_exp_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_imp_reg_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/D_imp_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_pba_reg_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/D_pba_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_cba_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/D_cba.txt"), "Matrix"),"RectangularMatrixAlt")
    D_pba_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/D_pba.txt"), "Matrix"),"RectangularMatrixAlt")
    M_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/M.txt"), "Matrix"),"RectangularMatrixAlt")
    S_Y_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/S_Y.txt"), "Matrix"),"RectangularMatrixAlt")
    S_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/S.txt"), "Matrix"),"RectangularMatrixAlt")
    F_Y_satellite = transformToNamedArray(parseData(string(IOTfile, "/satellite/F_Y.txt"), "Matrix"),"RectangularMatrixAlt")
    unit_satellite = CSV.read(string(IOTfile, "/satellite/unit.txt"), DataFrame)

    # Impacts accounts
    F_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/F.txt"), "Matrix"),"RectangularMatrixAlt")
    D_cba_reg_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/D_cba_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_exp_reg_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/D_exp_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_imp_reg_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/D_imp_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_pba_reg_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/D_pba_reg.txt"), "Country"),"RectangularMatrixAlt")
    D_cba_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/D_cba.txt"), "Matrix"),"RectangularMatrixAlt")
    D_pba_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/D_pba.txt"), "Matrix"),"RectangularMatrixAlt")
    M_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/M.txt"), "Matrix"),"RectangularMatrixAlt")
    S_Y_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/S_Y.txt"), "Matrix"),"RectangularMatrixAlt")
    S_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/S.txt"), "Matrix"),"RectangularMatrixAlt")
    F_Y_impacts = transformToNamedArray(parseData(string(IOTfile, "/impacts/F_Y.txt"), "Matrix"),"RectangularMatrixAlt")
    unit_impacts = CSV.read(string(IOTfile, "/impacts/unit.txt"), DataFrame)

    satellite = Satellite(F = F_satellite,
                        D_cba_reg = D_cba_reg_satellite,
                        D_exp_reg = D_exp_reg_satellite,
                        D_imp_reg = D_imp_reg_satellite,
                        D_pba_reg = D_pba_reg_satellite,
                        D_cba = D_cba_satellite,
                        D_pba = D_pba_satellite,
                        M = M_satellite,
                        S_Y = S_Y_satellite,
                        S = S_satellite,
                        F_Y = F_Y_satellite,
                        unit = unit_satellite)

    impacts = Impacts(F = F_impacts,
                    D_cba_reg = D_cba_reg_impacts,
                    D_exp_reg = D_exp_reg_impacts,
                    D_imp_reg = D_imp_reg_impacts,
                    D_pba_reg = D_pba_reg_impacts,
                    D_cba = D_cba_impacts,
                    D_pba = D_pba_impacts,
                    M = M_impacts,
                    S_Y = S_Y_impacts,
                    S = S_impacts,
                    F_Y = F_Y_impacts,
                    unit = unit_impacts)

    return Exiobase3(Z = Z,
                    Y = Y, 
                    x = x,
                    A = A,
                    satellite = satellite,
                    impacts = impacts,
                    unit = unit)
end



"""
    function parseData()
Function to load correctly the txt file.
"""
function parseData(file::String,type::String)

    if type == "Matrix"
        data = CSV.read(file, DataFrame; header = [1,2], skipto = 4)   
    else
        data = CSV.read(file, DataFrame)
    end   

    return data
end

"""
    function transformToNamedArray()
Function to transform the DataFrame parsed from Exiobase3 data in NamedMatrix for computations.
"""
function transformToNamedArray(df::DataFrame, type::String)
    if type == "SquareMatrix"
        ids = names(df[:,3:end]);

        content = Matrix(df[:,3:end]);

        newMatrix = NamedArray(content, (ids, ids), ("From", "To"));

    elseif type == "RectangularMatrix"
        col_names = names(df[:,3:end]);

        row_names = string.(df[:,1],"_",df[:,2]);

        content = Matrix(df[:,3:end]);

        newMatrix = NamedArray(content, (row_names, col_names), ("From", "To"));
    
    elseif type == "RectangularMatrixAlt"

        col_names = names(df[:,2:end]);

        row_names = df[:,1];

        content = Matrix(df[:,2:end]);


        newMatrix = NamedArray(content, (row_names, col_names), ("From","To"))

    else
        ids = df[1:end, 1:2]
        ids = string.(ids[:,1], "-", ids[:,2])

        content = (df[:,3])

        newMatrix = NamedArray(content, ids, ("From","To"))
    end
    
    return newMatrix
end
