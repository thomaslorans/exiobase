# This file contains functions for estimating Scope 1, 2 and 3 upstream.

"""
    Impacts
A data structure representing the various impacts.
"""
Base.@kwdef mutable struct Scopes
    x::NamedArray # Gross output 
    scope1::NamedArray # Scope 1 impacts 
    scope2::NamedArray # Scope 2 impacts
    scope3Upstream::NamedArray # Scope 3 upstream 
end