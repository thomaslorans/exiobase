# This function contains Input Output Modelling functions.


"""
    InputOutputModel
A data structure to represent the basic Input Output Model.
"""
Base.@kwdef mutable struct InputOutputModel
    A::NamedArray # Coefficient matrix 
    L::NamedArray # Leontieff matrix 
    LInv::NamedArray # Leontieff inverse matrix 
end



"""
    function getIOModel()
"""
function getIOModel(exio3::Exiobase3)

    return InputOutputModel(A = getAMatrix(exio3.x, exio3.Z),
                            L = getLeontieff(A),
                            Linv = inv(L))
end

"""
    function getAMatrix()
This function returns the coefficient matrix.
"""
function getAMatrix(x::NamedArray, Z::NamedArray)
    xDiag = fromVectorToDiagMatrix(x)
    A = Z * inv(xDiag)
    return A
end

"""
    function getLeontieff()
This function returns the Leontieff matrix (see Roger 2009 Chapter 2).
"""
function getLeontieff(A::NamedArray)
    identityMatrix = Matrix{Float64}(I, size(A, 1), size(A,2))
    L = identityMatrix - A 
    return L
end