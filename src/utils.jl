# This function contains utilities for the package.



Base.@kwdef mutable struct Remediation
    x::DataFrame # cleaned DataFrame
    ids::Vector # IDs to be kept  
end


"""
    fromVectorToDiagMatrix()
This function transform a vector to a diagonalised matrix.
"""
function fromVectorToDiagMatrix(vector::NamedArray)
    ids = names(vector, 1)
    M = vector[:,1]
    M = Diagonal(M)
    M = NamedArray(M, (ids, ids), ("From","To"))

    return M
end

"""
    function remediation()
This function drops the rows with value 0 in x (inconsistent / incoherent data) and return the cleaned dataframe and the ids to be dropped.
"""
function remediation(x::DataFrame)

    x[!,"indout"] = replace(x[:,"indout"], 0 => missing)
    x = dropmissing(x)
    ids = string.(x[:,1],"_",x[:,2])

    return Remediation(x = x,
                        ids = ids)
end


"""
    function cleanMatrix()
This function clean the matrices (ie. drop the value with no data in x and accordingly the sectors in the other matrices in order to be able to do the IO computation process).
"""
function cleanMatrix(df::DataFrame, remediation::Remediation)
    
end