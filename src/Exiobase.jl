module Exiobase

    """
        Exiobase 
    A Julia package for parsing Exiobase3 data.
    [Exiobase repository](https://github.com/JuliaImpact/Exiobase.jl)
    """
    
    using CSV 
    using DataFrames
    using ProgressMeter
    using NamedArrays
    using LinearAlgebra

    export

    # Types
    Exiobase3,
    Satellite,
    InputOutputModel,
    Scopes,


    # File IO
    loadData,
    transformToNamedArray,
    parseData,

    # Utils 
    fromVectorToDiagMatrix,

    # IO models
    getAMatrix,
    getIOModel,
    getLeontieff


    include("fileio.jl")
    include("scopes.jl")
    include("utils.jl")
    include("iomodels.jl")
    
    function __init__()

        nothing
    end

end
