using Exiobase
using DataFrames
using NamedArrays

using CSV

data = string("test/data/IOT_2019_pxp","/satellite/industries.txt")
test_X = CSV.read(data, DataFrame)

IOTfile = "test/data/IOT_2019_pxp"
df = (parseData(string(IOTfile, "/x.txt"), "Vector"))

df[!,"indout"] = replace(df[:,"indout"], 0 => missing)

df_2 = dropmissing(df)

test = loadData("test/data/IOT_2019_pxp")

names(test.impacts.S,2)


# testTransf = transformToNamedArray(test)


test.satellite.unit
test = df_2





