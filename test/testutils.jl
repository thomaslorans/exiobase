using Exiobase
using DataFrames
using NamedArrays

using CSV

exio3 = loadData("test/data/IOT_2019_pxp")

xx = coalesce(exio3.x)
test = fromVectorToDiagMatrix(xx)
names(test, 2)
size(test)
test_inv = inv(test)