using Documenter
using Exiobase

push!(LOAD_PATH,"../src/")
makedocs(
    sitename = "Exiobase.jl",
    checkdocs =:exports,
    pages = Any["index.md"],
    modules = [Exiobase],
    repo = "https://gitlab.com/thomaslorans/exiobase/blob/{commit}{path}#{line}"
)
