# Exiobase.jl

Documentation for Exiobase.jl


## Types

```@docs
Satellite
```
```@docs
Exiobase3
```
```@docs
Scopes
```
## Parsing the Exiobase3 files

```@docs
loadData
```

```@docs
transformToNamedArray
```